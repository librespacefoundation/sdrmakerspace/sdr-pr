------------------------------------------------------------------------------------------
-- HEIG-VD ///////////////////////////////////////////////////////////////////////////////
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
------------------------------------------------------------------------------------------
-- REDS Institute ////////////////////////////////////////////////////////////////////////
-- Reconfigurable Embedded Digital Systems
------------------------------------------------------------------------------------------
--
-- File                 : top.vhd
-- Author               : Convers Anthony
-- Date                 : 20.12.2019
--
-- Context              : SDR makerspace
--
------------------------------------------------------------------------------------------
-- Description : Top file of ZedBaord for PR test
--   
------------------------------------------------------------------------------------------
-- Dependencies : 
--   
------------------------------------------------------------------------------------------
-- Modifications :
-- Ver    Date        Engineer      Comments
-- 0.0    See header  ACS           Initial version.
--
------------------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity top is
  port (
    DDR_addr : inout STD_LOGIC_VECTOR ( 14 downto 0 );
    DDR_ba : inout STD_LOGIC_VECTOR ( 2 downto 0 );
    DDR_cas_n : inout STD_LOGIC;
    DDR_ck_n : inout STD_LOGIC;
    DDR_ck_p : inout STD_LOGIC;
    DDR_cke : inout STD_LOGIC;
    DDR_cs_n : inout STD_LOGIC;
    DDR_dm : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dq : inout STD_LOGIC_VECTOR ( 31 downto 0 );
    DDR_dqs_n : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dqs_p : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_odt : inout STD_LOGIC;
    DDR_ras_n : inout STD_LOGIC;
    DDR_reset_n : inout STD_LOGIC;
    DDR_we_n : inout STD_LOGIC;
    FIXED_IO_ddr_vrn : inout STD_LOGIC;
    FIXED_IO_ddr_vrp : inout STD_LOGIC;
    FIXED_IO_mio : inout STD_LOGIC_VECTOR ( 53 downto 0 );
    FIXED_IO_ps_clk : inout STD_LOGIC;
    FIXED_IO_ps_porb : inout STD_LOGIC;
    FIXED_IO_ps_srstb : inout STD_LOGIC;
    btns_5bits_tri_i : in STD_LOGIC_VECTOR ( 4 downto 0 );
    DIP_switch_8bits : in STD_LOGIC_VECTOR ( 7 downto 0 );
    LEDs_8bits        : out std_logic_vector(7 downto 0)
    );
end entity top;

architecture rtl of top is

  component system_wrapper is
  port (
    DDR_cas_n : inout STD_LOGIC;
    DDR_cke : inout STD_LOGIC;
    DDR_ck_n : inout STD_LOGIC;
    DDR_ck_p : inout STD_LOGIC;
    DDR_cs_n : inout STD_LOGIC;
    DDR_reset_n : inout STD_LOGIC;
    DDR_odt : inout STD_LOGIC;
    DDR_ras_n : inout STD_LOGIC;
    DDR_we_n : inout STD_LOGIC;
    DDR_ba : inout STD_LOGIC_VECTOR ( 2 downto 0 );
    DDR_addr : inout STD_LOGIC_VECTOR ( 14 downto 0 );
    DDR_dm : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dq : inout STD_LOGIC_VECTOR ( 31 downto 0 );
    DDR_dqs_n : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dqs_p : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    FIXED_IO_mio : inout STD_LOGIC_VECTOR ( 53 downto 0 );
    FIXED_IO_ddr_vrn : inout STD_LOGIC;
    FIXED_IO_ddr_vrp : inout STD_LOGIC;
    FIXED_IO_ps_srstb : inout STD_LOGIC;
    FIXED_IO_ps_clk : inout STD_LOGIC;
    FIXED_IO_ps_porb : inout STD_LOGIC;
    M03_AXI_araddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
    M03_AXI_arprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    M03_AXI_arready : in STD_LOGIC_VECTOR ( 0 to 0 );
    M03_AXI_arvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    M03_AXI_awaddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
    M03_AXI_awprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    M03_AXI_awready : in STD_LOGIC_VECTOR ( 0 to 0 );
    M03_AXI_awvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    M03_AXI_bready : out STD_LOGIC_VECTOR ( 0 to 0 );
    M03_AXI_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    M03_AXI_bvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
    M03_AXI_rdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    M03_AXI_rready : out STD_LOGIC_VECTOR ( 0 to 0 );
    M03_AXI_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    M03_AXI_rvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
    M03_AXI_wdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    M03_AXI_wready : in STD_LOGIC_VECTOR ( 0 to 0 );
    M03_AXI_wstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M03_AXI_wvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    leds_8bits_tri_o : out STD_LOGIC_VECTOR ( 7 downto 0 );
    sws_8bits_tri_i : in STD_LOGIC_VECTOR ( 7 downto 0 );
    btns_5bits_tri_i : in STD_LOGIC_VECTOR ( 4 downto 0 );
    decouple : in STD_LOGIC;
    FCLK_CLK0 : out STD_LOGIC;
    Reset_n : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component system_wrapper;

signal clock            : std_logic;
signal reset_n          : std_logic;
signal reset            : std_logic;
signal compteur         : unsigned(31 downto 0);

signal s_led_zero_on    : std_logic;
signal s_led_one_on     : std_logic;
signal s_led_two_on     : std_logic;
signal s_led_three_on   : std_logic;

signal pr_led_two_on     : std_logic;
signal pr_led_three_on   : std_logic;

signal led_0_s    : std_logic;
signal led_1_s    : std_logic;
signal led_2_s    : std_logic;
signal led_3_s    : std_logic;
signal led_8_bit_ps_n     : std_logic_vector(7 downto 0);
signal sws_8bits_tri_n     : std_logic_vector(7 downto 0);

signal decouple_s       : std_logic;
signal freeze           : std_logic;

--AXI Bus
signal M03_AXI_araddr     : std_logic_vector(31 downto 0);
signal M03_AXI_arprot     : std_logic_vector(2 downto 0);
signal M03_AXI_arready    : std_logic_vector(0 to 0);
signal M03_AXI_arvalid    : std_logic_vector(0 to 0);
signal M03_AXI_awaddr     : std_logic_vector(31 downto 0);
signal M03_AXI_awprot     : std_logic_vector(2 downto 0);
signal M03_AXI_awready    : std_logic_vector(0 to 0);
signal M03_AXI_awvalid    : std_logic_vector(0 to 0);
signal M03_AXI_bready     : std_logic_vector(0 to 0);
signal M03_AXI_bresp      : std_logic_vector(1 downto 0);
signal M03_AXI_bvalid     : std_logic_vector(0 to 0);
signal M03_AXI_rdata      : std_logic_vector(31 downto 0);
signal M03_AXI_rready     : std_logic_vector(0 to 0);
signal M03_AXI_rresp      : std_logic_vector(1 downto 0);
signal M03_AXI_rvalid     : std_logic_vector(0 to 0);
signal M03_AXI_wdata      : std_logic_vector(31 downto 0);
signal M03_AXI_wready     : std_logic_vector(0 to 0);
signal M03_AXI_wstrb      : std_logic_vector(3 downto 0);
signal M03_AXI_wvalid     : std_logic_vector(0 to 0);

begin

reset <= not reset_n;

LEDs_8bits(3 downto 0) <= led_8_bit_ps_n(3 downto 0);
LEDs_8bits(4) <= led_0_s;
LEDs_8bits(5) <= led_1_s;
LEDs_8bits(6) <= led_2_s;
LEDs_8bits(7) <= led_3_s;
sws_8bits_tri_n <= DIP_switch_8bits;

decouple_s <= sws_8bits_tri_n(0);
freeze <= decouple_s;

system_i: component system_wrapper
     port map (
      DDR_addr(14 downto 0) => DDR_addr(14 downto 0),
      DDR_ba(2 downto 0) => DDR_ba(2 downto 0),
      DDR_cas_n => DDR_cas_n,
      DDR_ck_n => DDR_ck_n,
      DDR_ck_p => DDR_ck_p,
      DDR_cke => DDR_cke,
      DDR_cs_n => DDR_cs_n,
      DDR_dm(3 downto 0) => DDR_dm(3 downto 0),
      DDR_dq(31 downto 0) => DDR_dq(31 downto 0),
      DDR_dqs_n(3 downto 0) => DDR_dqs_n(3 downto 0),
      DDR_dqs_p(3 downto 0) => DDR_dqs_p(3 downto 0),
      DDR_odt => DDR_odt,
      DDR_ras_n => DDR_ras_n,
      DDR_reset_n => DDR_reset_n,
      DDR_we_n => DDR_we_n,
      FCLK_CLK0 => clock,
      FIXED_IO_ddr_vrn => FIXED_IO_ddr_vrn,
      FIXED_IO_ddr_vrp => FIXED_IO_ddr_vrp,
      FIXED_IO_mio(53 downto 0) => FIXED_IO_mio(53 downto 0),
      FIXED_IO_ps_clk => FIXED_IO_ps_clk,
      FIXED_IO_ps_porb => FIXED_IO_ps_porb,
      FIXED_IO_ps_srstb => FIXED_IO_ps_srstb,
      M03_AXI_araddr(31 downto 0) => M03_AXI_araddr(31 downto 0),
      M03_AXI_arprot(2 downto 0) => M03_AXI_arprot(2 downto 0),
      M03_AXI_arready(0) => M03_AXI_arready(0),
      M03_AXI_arvalid(0) => M03_AXI_arvalid(0),
      M03_AXI_awaddr(31 downto 0) => M03_AXI_awaddr(31 downto 0),
      M03_AXI_awprot(2 downto 0) => M03_AXI_awprot(2 downto 0),
      M03_AXI_awready(0) => M03_AXI_awready(0),
      M03_AXI_awvalid(0) => M03_AXI_awvalid(0),
      M03_AXI_bready(0) => M03_AXI_bready(0),
      M03_AXI_bresp(1 downto 0) => M03_AXI_bresp(1 downto 0),
      M03_AXI_bvalid(0) => M03_AXI_bvalid(0),
      M03_AXI_rdata(31 downto 0) => M03_AXI_rdata(31 downto 0),
      M03_AXI_rready(0) => M03_AXI_rready(0),
      M03_AXI_rresp(1 downto 0) => M03_AXI_rresp(1 downto 0),
      M03_AXI_rvalid(0) => M03_AXI_rvalid(0),
      M03_AXI_wdata(31 downto 0) => M03_AXI_wdata(31 downto 0),
      M03_AXI_wready(0) => M03_AXI_wready(0),
      M03_AXI_wstrb(3 downto 0) => M03_AXI_wstrb(3 downto 0),
      M03_AXI_wvalid(0) => M03_AXI_wvalid(0),
      Reset_n(0) => reset_n,
      decouple => decouple_s,
      btns_5bits_tri_i(4 downto 0) => btns_5bits_tri_i(4 downto 0),
      leds_8bits_tri_o(7 downto 0) => led_8_bit_ps_n(7 downto 0),
      sws_8bits_tri_i(7 downto 0) => sws_8bits_tri_n(7 downto 0)
    );

process (reset, clock)
begin 
    if (reset = '1') then
    	 led_0_s <= '0';
         led_1_s <= '0';
         led_2_s <= '0';
         led_3_s <= '0';
    elsif(clock'event and clock = '1' ) then
    	 led_0_s <= s_led_zero_on;
    	 led_1_s <= s_led_one_on;
    	 led_2_s <= s_led_two_on;
    	 led_3_s <= s_led_three_on;
    end if;
end process;

process (reset, clock)
begin 
    if (reset = '1') then
    	 compteur <= (others => '0');
    elsif(clock'event and clock = '1' ) then
    	 compteur <= compteur + 1;
    end if;
end process;

s_led_zero_on <= compteur(23);
s_led_one_on <= compteur(24);

s_led_two_on <= '1' when freeze='1' else pr_led_two_on;
s_led_three_on <= '1' when freeze='1' else pr_led_three_on;
	
u_ip_led : entity work.led_Int_v1_0 
    port map (
        led_1_o             => pr_led_two_on,
        led_2_o             => pr_led_three_on,
        s00_axi_aclk        => clock,
        s00_axi_aresetn     => reset_n,
        s00_axi_awaddr      => M03_AXI_awaddr(3 downto 0),
        s00_axi_awprot      => M03_AXI_awprot,
        s00_axi_awvalid     => M03_AXI_awvalid(0),
        s00_axi_awready     => M03_AXI_awready(0),
        s00_axi_wdata       => M03_AXI_wdata,
        s00_axi_wstrb       => M03_AXI_wstrb,
        s00_axi_wvalid      => M03_AXI_wvalid(0),
        s00_axi_wready      => M03_AXI_wready(0),
        s00_axi_bresp       => M03_AXI_bresp,
        s00_axi_bvalid      => M03_AXI_bvalid(0),
        s00_axi_bready      => M03_AXI_bready(0),
        s00_axi_araddr      => M03_AXI_araddr(3 downto 0),
        s00_axi_arprot      => M03_AXI_arprot,
        s00_axi_arvalid     => M03_AXI_arvalid(0),
        s00_axi_arready     => M03_AXI_arready(0),
        s00_axi_rdata       => M03_AXI_rdata,
        s00_axi_rresp       => M03_AXI_rresp,
        s00_axi_rvalid      => M03_AXI_rvalid(0),
        s00_axi_rready      => M03_AXI_rready(0)
      );


end rtl;
