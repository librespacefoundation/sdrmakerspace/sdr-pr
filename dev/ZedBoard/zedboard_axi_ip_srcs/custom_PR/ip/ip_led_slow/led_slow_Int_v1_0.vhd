------------------------------------------------------------------------------------------
-- HEIG-VD ///////////////////////////////////////////////////////////////////////////////
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
------------------------------------------------------------------------------------------
-- REDS Institute ////////////////////////////////////////////////////////////////////////
-- Reconfigurable Embedded Digital Systems
------------------------------------------------------------------------------------------
--
-- File                 : led_slow_Int_v1_0.vhd
-- Author               : Convers Anthony
-- Date                 : 20.12.2019
--
-- Context              : SDR makerspace
--
------------------------------------------------------------------------------------------
-- Description : Custom AXI IP Block (version slow)
--   
------------------------------------------------------------------------------------------
-- Dependencies : 
--   
------------------------------------------------------------------------------------------
-- Modifications :
-- Ver    Date        Engineer      Comments
-- 0.0    See header  ACS           Initial version.
--
------------------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity led_slow_Int_v1_0 is
	-- generic (
		-- -- Users to add parameters here

		-- -- User parameters ends
		-- -- Do not modify the parameters beyond this line


		-- -- Parameters of Axi Slave Bus Interface S00_AXI
		-- C_S00_AXI_DATA_WIDTH	: integer	:= 32;
		-- C_S00_AXI_ADDR_WIDTH	: integer	:= 4
	-- );
	port (
		-- Users to add ports here
        led_1_o     : out std_logic;
        led_2_o     : out std_logic;
		-- User ports ends
		-- Do not modify the ports beyond this line


		-- Ports of Axi Slave Bus Interface S00_AXI
		s00_axi_aclk	: in std_logic;
		s00_axi_aresetn	: in std_logic;
		s00_axi_awaddr	: in std_logic_vector(4-1 downto 0);
		s00_axi_awprot	: in std_logic_vector(2 downto 0);
		s00_axi_awvalid	: in std_logic;
		s00_axi_awready	: out std_logic;
		s00_axi_wdata	: in std_logic_vector(32-1 downto 0);
		s00_axi_wstrb	: in std_logic_vector((32/8)-1 downto 0);
		s00_axi_wvalid	: in std_logic;
		s00_axi_wready	: out std_logic;
		s00_axi_bresp	: out std_logic_vector(1 downto 0);
		s00_axi_bvalid	: out std_logic;
		s00_axi_bready	: in std_logic;
		s00_axi_araddr	: in std_logic_vector(4-1 downto 0);
		s00_axi_arprot	: in std_logic_vector(2 downto 0);
		s00_axi_arvalid	: in std_logic;
		s00_axi_arready	: out std_logic;
		s00_axi_rdata	: out std_logic_vector(32-1 downto 0);
		s00_axi_rresp	: out std_logic_vector(1 downto 0);
		s00_axi_rvalid	: out std_logic;
		s00_axi_rready	: in std_logic
	);
end led_slow_Int_v1_0;

architecture arch_imp of led_slow_Int_v1_0 is

	-- component declaration
	component led_slow_Int_v1_0_S00_AXI is
		generic (
		C_S_AXI_DATA_WIDTH	: integer	:= 32;
		C_S_AXI_ADDR_WIDTH	: integer	:= 4
		);
		port (
        slave_reg1: out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_ACLK	: in std_logic;
		S_AXI_ARESETN	: in std_logic;
		S_AXI_AWADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		S_AXI_AWPROT	: in std_logic_vector(2 downto 0);
		S_AXI_AWVALID	: in std_logic;
		S_AXI_AWREADY	: out std_logic;
		S_AXI_WDATA	: in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_WSTRB	: in std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
		S_AXI_WVALID	: in std_logic;
		S_AXI_WREADY	: out std_logic;
		S_AXI_BRESP	: out std_logic_vector(1 downto 0);
		S_AXI_BVALID	: out std_logic;
		S_AXI_BREADY	: in std_logic;
		S_AXI_ARADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		S_AXI_ARPROT	: in std_logic_vector(2 downto 0);
		S_AXI_ARVALID	: in std_logic;
		S_AXI_ARREADY	: out std_logic;
		S_AXI_RDATA	: out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_RRESP	: out std_logic_vector(1 downto 0);
		S_AXI_RVALID	: out std_logic;
		S_AXI_RREADY	: in std_logic
		);
	end component led_slow_Int_v1_0_S00_AXI;
    
signal clk	          : std_logic;
signal reset	      : std_logic;
signal led_register_s : std_logic_vector(31 downto 0);
signal cnt_s          : unsigned(31 downto 0);
signal led_1_s	      : std_logic;
signal led_2_s	      : std_logic;

begin

-- Instantiation of Axi Bus Interface S00_AXI
led_slow_Int_v1_0_S00_AXI_inst : led_slow_Int_v1_0_S00_AXI
	generic map (
		C_S_AXI_DATA_WIDTH	=> 32,
		C_S_AXI_ADDR_WIDTH	=> 4
	)
	port map (
        slave_reg1 => led_register_s,
		S_AXI_ACLK	=> s00_axi_aclk,
		S_AXI_ARESETN	=> s00_axi_aresetn,
		S_AXI_AWADDR	=> s00_axi_awaddr,
		S_AXI_AWPROT	=> s00_axi_awprot,
		S_AXI_AWVALID	=> s00_axi_awvalid,
		S_AXI_AWREADY	=> s00_axi_awready,
		S_AXI_WDATA	=> s00_axi_wdata,
		S_AXI_WSTRB	=> s00_axi_wstrb,
		S_AXI_WVALID	=> s00_axi_wvalid,
		S_AXI_WREADY	=> s00_axi_wready,
		S_AXI_BRESP	=> s00_axi_bresp,
		S_AXI_BVALID	=> s00_axi_bvalid,
		S_AXI_BREADY	=> s00_axi_bready,
		S_AXI_ARADDR	=> s00_axi_araddr,
		S_AXI_ARPROT	=> s00_axi_arprot,
		S_AXI_ARVALID	=> s00_axi_arvalid,
		S_AXI_ARREADY	=> s00_axi_arready,
		S_AXI_RDATA	=> s00_axi_rdata,
		S_AXI_RRESP	=> s00_axi_rresp,
		S_AXI_RVALID	=> s00_axi_rvalid,
		S_AXI_RREADY	=> s00_axi_rready
	);

	-- Add user logic here
    
clk <= s00_axi_aclk;
reset <= not s00_axi_aresetn;

led_1_o <= led_1_s;
led_2_o <= led_2_s;

process (reset, clk)
 	begin if (reset = '1') then
    	 cnt_s <= (others => '0');
         led_1_s <= '0';
         led_2_s <= '0';
    elsif(clk'event and clk = '1' ) then
    	 cnt_s <= cnt_s + 1;
         
         if led_register_s(16)='1' then
            led_1_s <= not led_register_s(0);
         else
            led_1_s <= cnt_s(28);
         end if;
         
         if led_register_s(17)='1' then
            led_2_s <= not led_register_s(1);
         else
            led_2_s <= cnt_s(28);
         end if;
         
    end if;
end process;
    
	-- User logic ends

end arch_imp;
