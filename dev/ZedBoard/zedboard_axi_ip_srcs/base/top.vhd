------------------------------------------------------------------------------------------
-- HEIG-VD ///////////////////////////////////////////////////////////////////////////////
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
------------------------------------------------------------------------------------------
-- REDS Institute ////////////////////////////////////////////////////////////////////////
-- Reconfigurable Embedded Digital Systems
------------------------------------------------------------------------------------------
--
-- File                 : top.vhd
-- Author               : Convers Anthony
-- Date                 : 20.12.2019
--
-- Context              : SDR makerspace
--
------------------------------------------------------------------------------------------
-- Description : Top file of ZedBaord for PR test
--   
------------------------------------------------------------------------------------------
-- Dependencies : 
--   
------------------------------------------------------------------------------------------
-- Modifications :
-- Ver    Date        Engineer      Comments
-- 0.0    See header  ACS           Initial version.
--
------------------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity top is
  port (
    DDR_addr : inout STD_LOGIC_VECTOR ( 14 downto 0 );
    DDR_ba : inout STD_LOGIC_VECTOR ( 2 downto 0 );
    DDR_cas_n : inout STD_LOGIC;
    DDR_ck_n : inout STD_LOGIC;
    DDR_ck_p : inout STD_LOGIC;
    DDR_cke : inout STD_LOGIC;
    DDR_cs_n : inout STD_LOGIC;
    DDR_dm : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dq : inout STD_LOGIC_VECTOR ( 31 downto 0 );
    DDR_dqs_n : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dqs_p : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_odt : inout STD_LOGIC;
    DDR_ras_n : inout STD_LOGIC;
    DDR_reset_n : inout STD_LOGIC;
    DDR_we_n : inout STD_LOGIC;
    FIXED_IO_ddr_vrn : inout STD_LOGIC;
    FIXED_IO_ddr_vrp : inout STD_LOGIC;
    FIXED_IO_mio : inout STD_LOGIC_VECTOR ( 53 downto 0 );
    FIXED_IO_ps_clk : inout STD_LOGIC;
    FIXED_IO_ps_porb : inout STD_LOGIC;
    FIXED_IO_ps_srstb : inout STD_LOGIC;
    btns_5bits_tri_i : in STD_LOGIC_VECTOR ( 4 downto 0 );
    DIP_switch_8bits : in STD_LOGIC_VECTOR ( 7 downto 0 );
    LEDs_8bits        : out std_logic_vector(7 downto 0)
    );
end entity top;

architecture rtl of top is

  component system_wrapper is
  port (
    DDR_cas_n : inout STD_LOGIC;
    DDR_cke : inout STD_LOGIC;
    DDR_ck_n : inout STD_LOGIC;
    DDR_ck_p : inout STD_LOGIC;
    DDR_cs_n : inout STD_LOGIC;
    DDR_reset_n : inout STD_LOGIC;
    DDR_odt : inout STD_LOGIC;
    DDR_ras_n : inout STD_LOGIC;
    DDR_we_n : inout STD_LOGIC;
    DDR_ba : inout STD_LOGIC_VECTOR ( 2 downto 0 );
    DDR_addr : inout STD_LOGIC_VECTOR ( 14 downto 0 );
    DDR_dm : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dq : inout STD_LOGIC_VECTOR ( 31 downto 0 );
    DDR_dqs_n : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dqs_p : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    FIXED_IO_mio : inout STD_LOGIC_VECTOR ( 53 downto 0 );
    FIXED_IO_ddr_vrn : inout STD_LOGIC;
    FIXED_IO_ddr_vrp : inout STD_LOGIC;
    FIXED_IO_ps_srstb : inout STD_LOGIC;
    FIXED_IO_ps_clk : inout STD_LOGIC;
    FIXED_IO_ps_porb : inout STD_LOGIC;
    leds_8bits_tri_o : out STD_LOGIC_VECTOR ( 7 downto 0 );
    sws_8bits_tri_i : in STD_LOGIC_VECTOR ( 7 downto 0 );
    btns_5bits_tri_i : in STD_LOGIC_VECTOR ( 4 downto 0 );
    FCLK_CLK0 : out STD_LOGIC;
    Reset_n : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component system_wrapper;

signal clock            : std_logic;
signal reset_n          : std_logic;
signal reset            : std_logic;
signal compteur         : unsigned(31 downto 0);

signal s_led_zero_on    : std_logic;
signal s_led_one_on     : std_logic;
signal s_led_two_on     : std_logic;
signal s_led_three_on   : std_logic;

signal pr_led_two_on     : std_logic;
signal pr_led_three_on   : std_logic;

signal led_0_s    : std_logic;
signal led_1_s    : std_logic;
signal led_2_s    : std_logic;
signal led_3_s    : std_logic;
signal led_8_bit_ps_n     : std_logic_vector(7 downto 0);
signal sws_8bits_tri_n     : std_logic_vector(7 downto 0);

signal decouple_s       : std_logic;
signal freeze           : std_logic;


begin

reset <= not reset_n;

LEDs_8bits(3 downto 0) <= led_8_bit_ps_n(3 downto 0);
LEDs_8bits(4) <= led_0_s;
LEDs_8bits(5) <= led_1_s;
LEDs_8bits(6) <= led_2_s;
LEDs_8bits(7) <= led_3_s;
sws_8bits_tri_n <= DIP_switch_8bits;

decouple_s <= sws_8bits_tri_n(0);
freeze <= decouple_s;

system_i: component system_wrapper
     port map (
      DDR_addr(14 downto 0) => DDR_addr(14 downto 0),
      DDR_ba(2 downto 0) => DDR_ba(2 downto 0),
      DDR_cas_n => DDR_cas_n,
      DDR_ck_n => DDR_ck_n,
      DDR_ck_p => DDR_ck_p,
      DDR_cke => DDR_cke,
      DDR_cs_n => DDR_cs_n,
      DDR_dm(3 downto 0) => DDR_dm(3 downto 0),
      DDR_dq(31 downto 0) => DDR_dq(31 downto 0),
      DDR_dqs_n(3 downto 0) => DDR_dqs_n(3 downto 0),
      DDR_dqs_p(3 downto 0) => DDR_dqs_p(3 downto 0),
      DDR_odt => DDR_odt,
      DDR_ras_n => DDR_ras_n,
      DDR_reset_n => DDR_reset_n,
      DDR_we_n => DDR_we_n,
      FCLK_CLK0 => clock,
      FIXED_IO_ddr_vrn => FIXED_IO_ddr_vrn,
      FIXED_IO_ddr_vrp => FIXED_IO_ddr_vrp,
      FIXED_IO_mio(53 downto 0) => FIXED_IO_mio(53 downto 0),
      FIXED_IO_ps_clk => FIXED_IO_ps_clk,
      FIXED_IO_ps_porb => FIXED_IO_ps_porb,
      FIXED_IO_ps_srstb => FIXED_IO_ps_srstb,
      Reset_n(0) => reset_n,
      btns_5bits_tri_i(4 downto 0) => btns_5bits_tri_i(4 downto 0),
      leds_8bits_tri_o(7 downto 0) => led_8_bit_ps_n(7 downto 0),
      sws_8bits_tri_i(7 downto 0) => sws_8bits_tri_n(7 downto 0)
    );

process (reset, clock)
begin 
    if (reset = '1') then
    	 led_0_s <= '0';
         led_1_s <= '0';
         led_2_s <= '0';
         led_3_s <= '0';
    elsif(clock'event and clock = '1' ) then
    	 led_0_s <= s_led_zero_on;
    	 led_1_s <= s_led_one_on;
    	 led_2_s <= s_led_two_on;
    	 led_3_s <= s_led_three_on;
    end if;
end process;

process (reset, clock)
begin 
    if (reset = '1') then
    	 compteur <= (others => '0');
    elsif(clock'event and clock = '1' ) then
    	 compteur <= compteur + 1;
    end if;
end process;

s_led_zero_on <= compteur(23);
s_led_one_on <= compteur(24);
	
s_led_two_on <= '1' when freeze='1' else pr_led_two_on;
s_led_three_on <= '1' when freeze='1' else pr_led_three_on;

pr_led_two_on <= '0';
pr_led_three_on <= '0';

end rtl;
