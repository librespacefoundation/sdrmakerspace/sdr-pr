------------------------------------------------------------------------------------------
-- HEIG-VD ///////////////////////////////////////////////////////////////////////////////
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
------------------------------------------------------------------------------------------
-- REDS Institute ////////////////////////////////////////////////////////////////////////
-- Reconfigurable Embedded Digital Systems
------------------------------------------------------------------------------------------
--
-- File                 : blinking_led_empty.vhd
-- Author               : Convers Anthony
-- Date                 : 06.12.2019
--
-- Context              : SDR makerspace
--
------------------------------------------------------------------------------------------
-- Description : blinking led (empty version) component
--   
------------------------------------------------------------------------------------------
-- Dependencies : 
--   
------------------------------------------------------------------------------------------
-- Modifications :
-- Ver    Date        Engineer      Comments
-- 0.0    See header  ACS           Initial version.
--
------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity blinking_led_empty is
  port (
    reset           : in  std_logic;
    clk             : in  std_logic;
    led_o     : out std_logic
    );
end entity blinking_led_empty;

architecture rtl of blinking_led_empty is

signal cnt_s                 : unsigned(31 downto 0);

begin

 process (reset, clk)
 	 begin if (reset = '1') then
    	 cnt_s <= (others => '0');
    elsif(clk'event and clk = '1' ) then
    	 cnt_s <= cnt_s + 1;
         
    end if;
end process;

    led_o <= '0';

end rtl;
