------------------------------------------------------------------------------------------
-- HEIG-VD ///////////////////////////////////////////////////////////////////////////////
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
------------------------------------------------------------------------------------------
-- REDS Institute ////////////////////////////////////////////////////////////////////////
-- Reconfigurable Embedded Digital Systems
------------------------------------------------------------------------------------------
--
-- File                 : top.vhd
-- Author               : Convers Anthony
-- Date                 : 06.12.2019
--
-- Context              : SDR makerspace
--
------------------------------------------------------------------------------------------
-- Description : Top file of ZedBaord for PR test
--   
------------------------------------------------------------------------------------------
-- Dependencies : 
--   
------------------------------------------------------------------------------------------
-- Modifications :
-- Ver    Date        Engineer      Comments
-- 0.0    See header  ACS           Initial version.
--
------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity top is
  port (
    GCLK           : in  std_logic;
    reset_i        : in  std_logic;
    LEDs_8bits     : out std_logic_vector(7 downto 0)
    );
end entity top;

architecture rtl of top is

component blinking_led is
  port (
    reset     : in  std_logic;
    clk       : in  std_logic;
    led_o     : out std_logic
    );
end component blinking_led;

signal clock            : std_logic;
signal reset            : std_logic;
signal compteur         : unsigned(31 downto 0);

signal s_led_zero_on    : std_logic;
signal s_led_one_on     : std_logic;
signal s_led_two_on     : std_logic;
signal s_led_three_on   : std_logic;

signal pr_led_two_on     : std_logic;
signal pr_led_three_on   : std_logic;

signal led_0_s    : std_logic;
signal led_1_s    : std_logic;
signal led_2_s    : std_logic;
signal led_3_s    : std_logic;


signal freeze           : std_logic;
signal pr_ip_status     : std_logic_vector(2 downto 0);

begin

clock <= GCLK;
reset <= reset_i;

LEDs_8bits(0) <= led_0_s;
LEDs_8bits(1) <= led_1_s;
LEDs_8bits(2) <= led_2_s;
LEDs_8bits(3) <= led_3_s;
LEDs_8bits(7 downto 4) <= "1111";

freeze <= reset;


process (reset, clock)
begin 
    if (reset = '1') then
    	 led_0_s <= '0';
         led_1_s <= '0';
         led_2_s <= '0';
         led_3_s <= '0';
    elsif(clock'event and clock = '1' ) then
    	 led_0_s <= s_led_zero_on;
    	 led_1_s <= s_led_one_on;
    	 led_2_s <= s_led_two_on;
    	 led_3_s <= s_led_three_on;
    end if;
end process;

process (reset, clock)
begin 
    if (reset = '1') then
    	 compteur <= (others => '0');
    elsif(clock'event and clock = '1' ) then
    	 compteur <= compteur + 1;
    end if;
end process;

s_led_zero_on <= compteur(23);
s_led_one_on <= compteur(24);

	u_blinking_led : blinking_led 
    port map (
        reset         => reset,
        clk           => clock,
        led_o      	 => pr_led_two_on
      );
	
	s_led_two_on <= '1' when freeze='1' else pr_led_two_on;
	
	
	u_blink_2 : blinking_led 
    port map (
        reset         => reset,
        clk           => clock,
        led_o      	 => pr_led_three_on
      );
	
	s_led_three_on <= '1' when freeze='1' else pr_led_three_on;
    
end rtl;
