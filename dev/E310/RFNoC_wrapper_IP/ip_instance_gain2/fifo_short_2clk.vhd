------------------------------------------------------------------------------------------
-- HEIG-VD ///////////////////////////////////////////////////////////////////////////////
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
------------------------------------------------------------------------------------------
-- REDS Institute ////////////////////////////////////////////////////////////////////////
-- Reconfigurable Embedded Digital Systems
------------------------------------------------------------------------------------------
--
-- File                 : fifo_short_2clk.vhd
-- Author               : Convers Anthony
-- Date                 : 30.01.2020
--
-- Context              : SDR_makerspace
--
------------------------------------------------------------------------------------------
-- Description : fifo_short_2clk IP block instance
--   
------------------------------------------------------------------------------------------
-- Dependencies : 
--   
------------------------------------------------------------------------------------------
-- Modifications :
-- Ver    Date        Engineer      Comments
-- 0.0    See header  ACS           Initial version.
--
------------------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY fifo_short_2clk IS
  PORT (
    rst : IN STD_LOGIC;
    wr_clk : IN STD_LOGIC;
    rd_clk : IN STD_LOGIC;
    din : IN STD_LOGIC_VECTOR(71 DOWNTO 0);
    wr_en : IN STD_LOGIC;
    rd_en : IN STD_LOGIC;
    dout : OUT STD_LOGIC_VECTOR(71 DOWNTO 0);
    full : OUT STD_LOGIC;
    empty : OUT STD_LOGIC;
    rd_data_count : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    wr_data_count : OUT STD_LOGIC_VECTOR(5 DOWNTO 0)
  );
END fifo_short_2clk;

ARCHITECTURE fifo_short_2clk_arch OF fifo_short_2clk IS

    COMPONENT fifo_short_2clk_pr2
      PORT (
        rst : IN STD_LOGIC;
        wr_clk : IN STD_LOGIC;
        rd_clk : IN STD_LOGIC;
        din : IN STD_LOGIC_VECTOR(71 DOWNTO 0);
        wr_en : IN STD_LOGIC;
        rd_en : IN STD_LOGIC;
        dout : OUT STD_LOGIC_VECTOR(71 DOWNTO 0);
        full : OUT STD_LOGIC;
        empty : OUT STD_LOGIC;
        rd_data_count : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
        wr_data_count : OUT STD_LOGIC_VECTOR(5 DOWNTO 0)
      );
    END COMPONENT;

BEGIN

    i_fifo_short_2clk_pr2 : fifo_short_2clk_pr2
      PORT MAP (
        rst => rst,
        wr_clk => wr_clk,
        rd_clk => rd_clk,
        din => din,
        wr_en => wr_en,
        rd_en => rd_en,
        dout => dout,
        full => full,
        empty => empty,
        rd_data_count => rd_data_count,
        wr_data_count => wr_data_count
      );

END fifo_short_2clk_arch;
